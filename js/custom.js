$(".toglebtn").on("click", function() {
  var _trg = $(this).attr("data-toggle");
  var _trg2 = $(this);
  var element = document.getElementById("togle");

  if ($("#" + _trg).css("display") == "block") {
      $("#" + _trg).slideUp(100);
      _trg2.html('事前相談フローを表示する');
      element.classList.add("active");
  } else {
      $("#" + _trg).slideDown(100);
      _trg2.html('非表示にする');
      element.classList.remove("active");
  }
  return false;
})
jQuery(document).ready(function(e) {
  var t = e("#backtotop");
  e(window).scroll(function() {
      e(this).scrollTop() >= 800 ? t.show(10).animate({
          opacity: "1"
      }, 10) : t.animate({
          opacity: "0"
      }, 10)
  });
  t.click(function(t) {
      t.preventDefault();
      e("html,body").animate({
          scrollTop: 0
      }, 400)
  })
})